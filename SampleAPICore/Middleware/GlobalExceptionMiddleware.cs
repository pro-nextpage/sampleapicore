﻿using EZExceptions.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.Owin;
using Newtonsoft.Json;
using SampleAPICore.Services;
using SampleAPICore.Services.Logging;
using System;
using System.Threading.Tasks;

namespace SampleAPICore.Middleware
{
    public class GlobalExceptionMiddleware /*: OwinMiddleware*/
    {
        private readonly RequestDelegate _next;
        private readonly EZContext _ezContext;

        public GlobalExceptionMiddleware(RequestDelegate next, EZContext ezContext) /*: base(next)*/
        {
            _next = next;
            _ezContext = ezContext;
        }
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (Exception exception)
            {
                WebApiExceptionResult result;

                if (exception is WebApiException webApiExcpetion)
                {
                    webApiExcpetion.RequestId = _ezContext.RequestId;

                    result = webApiExcpetion.ToResult();
                }
                else
                {
                    Logger.LogException(exception);

                    result = new WebApiExceptionBuilder()
                        .WithStatusCode(500)
                        .WithRequestId(_ezContext.RequestId)
                        .Build()
                        .ToResult();
                }

                var settings = new JsonSerializerSettings
                {
                    Formatting = Formatting.Indented,
                    NullValueHandling = NullValueHandling.Ignore
                };

                var json = JsonConvert.SerializeObject(result, settings);

                context.Response.Headers.Add("EZ-WebApiException", new[] { "true" });
                context.Response.StatusCode = result.StatusCode;
                await context.Response.WriteAsync(json);
            }
        }
    }
}
