﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Owin;
using SampleAPICore.Services.ConfigProvider;
using Serilog;
using Serilog.Context;
using Serilog.Events;
using System;
using System.Diagnostics;
using System.Net;

namespace SampleAPICore.Services.Logging
{
    public static class Logger
    {
        private static readonly Stopwatch ApplicationSw = new Stopwatch();

        public static void Initialize(IConfigProvider config)
        {
            var minimumLevel = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), config.LoggingMinimumLevel);

            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()             
                .EnrichWithCommonProperties(config.EnvironmentName)
                .IncludeFlurl()
                .MinimumLevel.Is(minimumLevel)
                .DestructureOwinModels()
                .ConsiderWritingToLoggly(config.LogglyToken)
                .CreateLogger();
        }

        public static void LogApplicationStarted()
        {
            Log.Information("Application started: \"{Application:l} {Version:l}\" [{Environment:l}:{Machine:l}]");

            ApplicationSw.Start();
        }

        public static void LogApplicationStopped()
        {
            ApplicationSw.Stop();

            var durationInMinutes = ApplicationSw.ElapsedMilliseconds / 60000;
            Log.ForContext("DurationInMinutes", durationInMinutes)
                .Information("Application stopped: \"{Application:l} {Version:l}\" [{Environment:l}:{Machine:l}] ({DurationInMinutes:n0} m)");

            Log.CloseAndFlush();
        }

        public static void LogRequestStarted(HttpContext context, Stopwatch stopwatch, EZContext ezContext)
        {
            LogContext.PushProperty("RequestID", ezContext.RequestId);
            LogContext.PushProperty("CorrelationID", ezContext.CorrelationId(context));

            var request = context.Request.Body;

            Log.ForContext("Request", request, true)
                .Verbose("Request started: {Method:l} {path}", context.Request.Method, context.Request.Path);

            stopwatch?.Start();
        }

        public static void LogRequestCompleted(HttpContext context, Stopwatch stopwatch)
        {
            stopwatch?.Stop();

            var durationInMilliseconds = stopwatch?.ElapsedMilliseconds;
            var response = context.Request.Body; //context.Response.ToLogModel();

            Log.ForContext("Response", response, true)
                .Verbose("Request completed: {Method:l} {path} [{StatusCode} {StatusDescription:l}] ({DurationInMilliseconds:n0} ms)",
                    context.Request.Method,
                    context.Request.Path,
                    context.Response.StatusCode,
                    ((HttpStatusCode)context.Response.StatusCode).ToString(),
                    durationInMilliseconds);
        }

        public static void LogException(Exception exception)
        {
            Log.ForContext("Exception", exception, true)
                .Error("{Message}", exception.Message);
        }
    }
}
