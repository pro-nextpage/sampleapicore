﻿using EZExceptions.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Owin;
using SampleAPICore.Services.ConfigProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleAPICore.Middleware
{
    public class APIKeyAuthenticationMiddleware
    {
        private readonly IConfigProvider _config;
        private readonly RequestDelegate _next;

        public APIKeyAuthenticationMiddleware(IConfigProvider config, RequestDelegate next) /*: base(next)*/
        {
            _next = next;
            _config = config;
        }

        public async Task Invoke(HttpContext context)
        {
            var apiKey = _config.SampleAPISiteKey;
            var authenticationNotNeeded = string.IsNullOrEmpty(apiKey) ||
                                          context.Request.Path.Value == "/" ||
                                          context.Request.Path.Value.StartsWith("/hangfire") ||
                                          context.Request.Path.Value.StartsWith("/swagger");

            if (authenticationNotNeeded)
            {
                await _next.Invoke(context);

                return;
            }

            var isAuthenticated = context.Request.Headers["X-APIKey"] == apiKey ||
                                  context.Request.Query["api_key"] == apiKey;

            if (isAuthenticated)
            {
                await _next.Invoke(context);

                return;
            }

            throw new WebApiExceptionBuilder()
                .WithError("Unauthorized")
                .WithStatusCode(401)
                .Build();
        }
    }
}
