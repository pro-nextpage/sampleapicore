﻿using Microsoft.AspNetCore.Http;
using Microsoft.Owin;
using System;

namespace SampleAPICore.Services
{
    public class EZContext
    {
        public EZContext()
        {
            RequestId = Guid.NewGuid().ToString();
        }

        public string RequestId { get; }

        public string CorrelationId(HttpContext httpContext)
        {
          //  var correlationId = string.IsNullOrEmpty(httpContext.Request.Headers["X-Correlation-ID"]);
                    //? RequestId
                    //: httpContext.Request.Headers["X-Correlation-ID"];
            return httpContext.Request.Headers["X-Correlation-ID"];

           // return correlationId;
        }
    }

}
