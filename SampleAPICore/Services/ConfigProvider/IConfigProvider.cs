﻿namespace SampleAPICore.Services.ConfigProvider
{
    public class IConfigProvider
    {
        public string DbConnectionString { get; set; }
        public string EnvironmentName { get; set; }
        public string LoggingMinimumLevel { get; set; }
        public string LogglyToken { get; set; }
        public string SampleAPISiteKey { get; set; }

        public string SecureIpAddress { get; set; }
        public string ServiceBusAPIKey { get; set; }
        public string ServiceBusRootURL { get; set; }        
        public string SiteName { get; set; }
        public string ServiceTwilioAccountSId { get; set; }
        public string ServiceTwilioAuthToken { get; set; }
        public string TwilioTextFrom { get; set; }

    }
}
