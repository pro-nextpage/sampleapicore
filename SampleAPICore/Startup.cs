using System;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using SampleAPICore.DependencyResolution;
using SampleAPICore.Middleware;
using SampleAPICore.Services;
using SampleAPICore.Services.ConfigProvider;
using SampleAPICore.Services.Hangfire;
using SampleAPICore.Services.Logging;
using SampleAPICore.Services.ResourceProvider;

namespace SampleAPICore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
           
        }
        
        public IConfiguration Configuration { get; }
        public IServiceProvider container { get; private set; }

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            var iconfig = Configuration.GetSection(nameof(IConfigProvider));
            services.Configure<IConfigProvider>(iconfig);           
            services.AddHangfire(c => c.UseMemoryStorage());          
            services.AddHangfireServer();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Sample API Core", Version = "v1" });
            });

        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var container = IoC.Initialize();
            var dependencyResolver = new StructureMapWebApiDependencyResolver(container);
            var httpConfig = new HttpConfiguration { DependencyResolver = dependencyResolver };
            var config = container.GetInstance<IConfigProvider>();
            var ezContext = container.GetInstance<EZContext>();
            var _configProvider = Configuration.GetSection("IConfigProvider").Get<IConfigProvider>();
            config = _configProvider;            
            var dbUp = new DbUpProcessor(config, container.GetInstance<IResourceProvider>(), true);
            var twilio = container.GetInstance<TwilioTexting>();

            Logger.Initialize(config);
            Logger.LogApplicationStarted();

            dbUp.EnsureDbVersion();
            twilio.Send(config.ServiceTwilioAccountSId,config.ServiceTwilioAuthToken);
            httpConfig.Filters.Add(new TrimStringsFilterAttribute());
            httpConfig.Services.Replace(typeof(IExceptionHandler), new PassthroughExceptionHandler());
            httpConfig.MapHttpAttributeRoutes();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            GlobalConfiguration.Configuration.UseActivator(new HangfireJobActivator(container));
            app.UseHangfireDashboard("/hangfire", new DashboardOptions { Authorization = new[] { new SecureIpAuthorizationFilter(container.GetInstance<IConfigProvider>().SecureIpAddress) } });
            app.UseHangfireServer();            
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Sample API Core");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseMiddleware<LoggingMiddleware>(container);
            app.UseMiddleware<GlobalExceptionMiddleware>(ezContext);
            app.UseMiddleware<APIKeyAuthenticationMiddleware>(config);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
