﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Owin;
using StructureMap;
using SampleAPICore.Services;
using SampleAPICore.Services.Logging;
using Microsoft.AspNetCore.Http;

namespace SampleAPICore.Middleware
{
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IContainer _container;

        public LoggingMiddleware(RequestDelegate next, IContainer container) /*: base(next)*/
        {
            _next = next;
            _container = container;
        }

        public  async Task Invoke(HttpContext context)
        {
            var isSwagger = context.Request.Path.Value.StartsWith("/swagger") ||
                            context.Request.Path.Value == "/";

            if (isSwagger)
            {
                await _next(context);

                return;
            }

            var stopwatch = new Stopwatch();
            var ezContext = _container.GetInstance<EZContext>();

            Logger.LogRequestStarted(context, stopwatch, ezContext);

            await _next.Invoke(context);

            Logger.LogRequestCompleted(context, stopwatch);
        }
    }
}


