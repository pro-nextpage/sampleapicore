﻿using Hangfire.Dashboard;
using Microsoft.Owin;


namespace SampleAPICore.Services.Hangfire
{
    public class SecureIpAuthorizationFilter : IDashboardAuthorizationFilter
    {
        private readonly string _secureIpAddress;

        public SecureIpAuthorizationFilter(string secureIpAddress)
        {
            _secureIpAddress = secureIpAddress;
        }

        public bool Authorize(DashboardContext context)
        {
            var httpContext = context.GetHttpContext();
            var userIpAddress = httpContext.Request.Headers["X-Forwarded-For"];// Need to get ip address

            var userIsLocal = userIpAddress == "127.0.0.1" || userIpAddress == "::1";
            var userIsSecure = userIpAddress == _secureIpAddress;

            if (userIsLocal || userIsSecure)
            {
                return true;
            }
            return false;
        }
    }
}
