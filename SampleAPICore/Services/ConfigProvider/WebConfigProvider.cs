﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleAPICore.Services.ConfigProvider
{
    public class WebConfigProvider/* : IConfigProvider*/
    {
        private IConfiguration configuration;
        public WebConfigProvider(IConfiguration iConfig)
        {
            configuration = iConfig;
        }
        public string SampleAPISiteKey => configuration.GetSection("IConfigProvider").GetSection("SampleAPISiteKey").Value;
        public string DbConnectionString => configuration.GetSection("IConfigProvider").GetSection("DbConnectionString").Value;

        public string EnvironmentName => configuration.GetSection("IConfigProvider").GetSection("EnvironmentName").Value;
        public string LoggingMinimumLevel => configuration.GetSection("IConfigProvider").GetSection("LoggingMinimumLevel").Value;
        public string LogglyToken => configuration.GetSection("IConfigProvider").GetSection("LogglyToken").Value;
        public string SecureIpAddress => configuration.GetSection("IConfigProvider").GetSection("SecureIpAddress").Value;
        public string ServiceBusAPIKey => configuration.GetSection("IConfigProvider").GetSection("ServiceBusAPIKey").Value;
        public string ServiceBusRootURL => configuration.GetSection("IConfigProvider").GetSection("ServiceBusRootURL").Value;
        public string SiteName => configuration.GetSection("IConfigProvider").GetSection("SiteName").Value;    
        public string ServiceTwilioAccountSId => configuration.GetSection("IConfigProvider").GetSection("ServiceTwilioAccountSId").Value;
        public string ServiceTwilioAuthToken => configuration.GetSection("IConfigProvider").GetSection("ServiceTwilioAuthToken").Value;
        public string TwilioTextFrom => configuration.GetSection("IConfigProvider").GetSection("TwilioTextFrom").Value;

    }
}
