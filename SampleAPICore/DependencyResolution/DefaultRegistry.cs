﻿using SampleAPICore.Services;
using SampleAPICore.Services.ConfigProvider;
using SampleAPICore.Services.ResourceProvider;
using EZBus;
using EZDataTools;
using EZPostgresDataTools;
using StructureMap.Configuration.DSL;
using StructureMap;

namespace SampleAPICore.DependencyResolution
{
    public class DefaultRegistry : Registry
    {
        public DefaultRegistry()
        {
            For<IConfigProvider>()
                .Use(x => new IConfigProvider());

            For<IDatabase>()
                .Use(x => new PostgresDatabase(x.GetInstance<IConfigProvider>().DbConnectionString));

            For<EZContext>()                
                .Use(x => new EZContext());

            For<IResourceProvider>()
                .Use(x => new EmbeddedResourceProvider());

            For<IServiceBus>()
                .Use(x => new WebBus(
                    x.GetInstance<IConfigProvider>().ServiceBusRootURL,
                    x.GetInstance<IConfigProvider>().ServiceBusAPIKey)
                );
            For<TwilioTexting>()
                .Use(x=>new TwilioTexting());
        }
    }
}
