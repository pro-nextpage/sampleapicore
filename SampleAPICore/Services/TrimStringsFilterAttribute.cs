﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using SampleAPICore.Extensions;

namespace SampleAPICore.Services
{
    public class TrimStringsFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            actionContext.ActionArguments.TrimStrings();
        }
    }
}
