﻿using SampleAPICore.Services.ConfigProvider;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace SampleAPICore.Services
{
    public class TwilioTexting
    {            
        public TwilioTexting()
        {

        }
        public void Send(string serviceTwilioAccountSId,string serviceTwilioAuthToken)
        {
            try
            {
                TwilioClient.Init(serviceTwilioAccountSId, serviceTwilioAuthToken);
            }
            catch (Exception ex)
            {
                Log.Error($"Texting failure: {ex.Message}");
                throw;
            }
        }
    }
}
