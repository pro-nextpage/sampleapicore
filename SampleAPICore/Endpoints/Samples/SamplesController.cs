using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SampleAPICore.Services.ConfigProvider;

namespace SampleAPICore.Controllers
{
    public partial class SamplesController : ControllerBase
    {
        private readonly IConfigProvider _appSettings;
        public SamplesController(IOptions<IConfigProvider> appSettings)
        {
            _appSettings = appSettings.Value;
        }

    }
}
