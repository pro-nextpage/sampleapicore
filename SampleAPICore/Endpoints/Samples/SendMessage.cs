using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SampleAPICore.Services;
using SampleAPICore.Services.ConfigProvider;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace SampleAPICore.Endpoints.Samples
{
    public partial class SamplesController
    {
        private readonly IConfigProvider _iconfig;
        public SamplesController(IOptions<IConfigProvider> iconfig)
        {
            _iconfig = iconfig.Value;
        }
        [HttpPost]
        [Route("Samples/SendMessage/")]
        public MessageResource Texting(string message, string to)
        {
            var sendMessage = MessageResource.Create(
                body: message,
                from: new Twilio.Types.PhoneNumber(_iconfig.TwilioTextFrom),
                to: new Twilio.Types.PhoneNumber(to)
            );
            return sendMessage;
        }
    }
}
