﻿using Microsoft.Owin.BuilderProperties;
using Owin;
using System;
using System.Threading;
namespace SampleAPICore.Validators
{
    public static class AppBuilderExtensions
    {
        public static void OnAppDisposing(this IAppBuilder app, Action callback)
        {
            var properties = new AppProperties(app.Properties);
            var token = properties.OnAppDisposing;

            if (token != CancellationToken.None)
            {
                token.Register(callback);
            }
        }
    }
}
